Command line instructions

Git global setup
	git config --global user.name "Mahmud Hasan"
	git config --global user.email "mahmud@datashall.com"

Create a new repository
	git clone https://gitlab.com/zero.fighter/dingo.git
	cd dingo
	touch README.md
	git add README.md
	git commit -m "add README"
	git push -u origin master

Existing folder
	cd existing_folder
	git init
	git remote add origin https://gitlab.com/zero.fighter/dingo.git
	git add .
	git commit -m "Initial commit"
	git push -u origin master

Existing Git repository
	cd existing_repo
	git remote rename origin old-origin
	git remote add origin https://gitlab.com/zero.fighter/dingo.git
	git push -u origin --all
	git push -u origin --tags

Status
	git status
	git log


Start guide link - https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
